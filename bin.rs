//! Rustjack Binary
//! Author: Drew McKinney
//! date:   2/25/22

extern crate mylib;

use std::io;

/// main function
/// get user input for variables
/// run simulation with given
pub fn main() {
    // prompt user for simulation variables
    //      or use default
    let start_chips;
    let n_decks;
    let n_hands;
    let starting_bet;
    let mut max_bet = 200;
    let mut lose_mult = 2.0;
    let result_vec: Vec<(u64, mylib::Result)>;

    let mut input_string = String::new();

    // variables to obtain from user
    //     starting_chips, n_decks, n_hands
    println!("Welcome to RustJack!");
    println!("How many chips are you starting with?: ");
    input_string.clear();
    io::stdin().read_line(&mut input_string).unwrap();
    //let string_in = input_string.trim();
    // keep trying to get correct input
    while input_string.trim().parse::<u64>().is_err() {
        println!("INCORRECT INPUT TRY AGAIN");
        input_string.clear();
        io::stdin().read_line(&mut input_string).unwrap();
    }
    start_chips = input_string.trim().parse::<u64>().unwrap();
    println!("Starting Chips: {}", start_chips);

    println!("How many decks are being used?: ");
    input_string.clear();
    io::stdin().read_line(&mut input_string).unwrap();
    while input_string.trim().parse::<u64>().is_err() {
        println!("INCORRECT INPUT TRY AGAIN");
        input_string.clear();
        io::stdin().read_line(&mut input_string).unwrap();
    }
    n_decks = input_string.trim().parse::<u64>().unwrap();
    println!("Number of Decks: {}", n_decks);

    println!("How many hands are being simulated?: ");
    input_string.clear();
    io::stdin().read_line(&mut input_string).unwrap();
    while input_string.trim().parse::<u64>().is_err() {
        println!("INCORRECT INPUT TRY AGAIN");
        input_string.clear();
        io::stdin().read_line(&mut input_string).unwrap();
    }
    n_hands = input_string.trim().parse::<u64>().unwrap();
    println!("Number of Hands: {}", n_hands);

    println!("Choose your starting bet: ");
    input_string.clear();
    io::stdin().read_line(&mut input_string).unwrap();
    while input_string.trim().parse::<u64>().is_err() {
        println!("INCORRECT INPUT TRY AGAIN");
        input_string.clear();
        io::stdin().read_line(&mut input_string).unwrap();
    }
    starting_bet = input_string.trim().parse::<u64>().unwrap();
    println!("Starting Bet: {}", starting_bet);

    let mut betting_strategy = mylib::BetStrategy::new(starting_bet, max_bet, lose_mult);
    let mut my_deck = mylib::Deck::new(n_decks);
    let mut move_rules = mylib::Rules::new();

    // ask for rule changes
    let mut still_modifying = true;
    while still_modifying {
        println!("Would you like to modify rules? (Y/N)");
        input_string.clear();
        io::stdin().read_line(&mut input_string).unwrap();
        //let inp = input_string.trim();
        while input_string.trim() != "y"
            && input_string.trim() != "Y"
            && input_string.trim() != "n"
            && input_string.trim() != "N"
        {
            println!("INCORRECT INPUT TRY AGAIN");
            input_string.clear();
            io::stdin().read_line(&mut input_string).unwrap();
        }
        if input_string.trim() == "y" || input_string.trim() == "Y" {
            // ask for rule to modify

            let player_val;
            let dealer_val;
            println!("MODIFY RULES:");

            // get player hand value for modification
            println!("PLAYER SHOWING VALUE: ");
            input_string.clear();
            io::stdin().read_line(&mut input_string).unwrap();
            while input_string.trim().parse::<u64>().is_err() {
                println!("INCORRECT INPUT TRY AGAIN");
                input_string.clear();
                io::stdin().read_line(&mut input_string).unwrap();
            }
            player_val = input_string.trim().parse::<u64>().unwrap();

            // get dealer hand value for modification
            println!("DEALER SHOWING VALUE: ");
            input_string.clear();
            io::stdin().read_line(&mut input_string).unwrap();
            while input_string.trim().parse::<u64>().is_err() {
                println!("INCORRECT INPUT TRY AGAIN");
                input_string.clear();
                io::stdin().read_line(&mut input_string).unwrap();
            }
            dealer_val = input_string.trim().parse::<u64>().unwrap();

            // get new move at chosen spot in matrix
            println!(
                "NEW MOVE RULE FOR CHOSEN PLAYER AND DEALER HANDS: 
                            (Hit/Stand/Double)"
            );
            input_string.clear();
            io::stdin().read_line(&mut input_string).unwrap();
            while input_string.trim() != "Hit"
                && input_string.trim() != "Stand"
                && input_string.trim() != "Double"
            {
                println!("INCORRECT INPUT TRY AGAIN");
                input_string.clear();
                io::stdin().read_line(&mut input_string).unwrap();
            }
            let mut new_move: mylib::Move = move_rules.det_move(player_val, dealer_val);
            if input_string.trim() == "Hit" {
                new_move = mylib::Move::Hit;
            } else if input_string.trim() == "Stand" {
                new_move = mylib::Move::Stand;
            } else if input_string.trim() == "Double" {
                new_move = mylib::Move::Double;
            } else {
                //error
            }

            println!(
                "OLD RULE: {:?}",
                move_rules.det_move(player_val, dealer_val)
            );
            move_rules.change_rule(player_val, dealer_val, new_move);
            println!(
                "NEW RULE: {:?}",
                move_rules.det_move(player_val, dealer_val)
            );
        } else if input_string.trim() == "n" || input_string.trim() == "N" {
            still_modifying = false;
        }
    }

    // modify betting strategy
    println!("Would you like to modify betting? (Y/N)");
    input_string.clear();
    io::stdin().read_line(&mut input_string).unwrap();
    //let inp = input_string.trim();
    while input_string.trim() != "y"
        && input_string.trim() != "Y"
        && input_string.trim() != "n"
        && input_string.trim() != "N"
    {
        println!("INCORRECT INPUT TRY AGAIN");
        input_string.clear();
        io::stdin().read_line(&mut input_string).unwrap();
    }

    if input_string.trim() == "y" || input_string.trim() == "Y" {
        println!("MAX BET: ");
        input_string.clear();
        io::stdin().read_line(&mut input_string).unwrap();
        while input_string.trim().parse::<u64>().is_err() {
            println!("INCORRECT INPUT TRY AGAIN");
            input_string.clear();
            io::stdin().read_line(&mut input_string).unwrap();
        }
        max_bet = input_string.trim().parse::<u64>().unwrap();

        println!("LOST HAND BET MULTIPLIER: ");
        input_string.clear();
        io::stdin().read_line(&mut input_string).unwrap();
        while input_string.trim().parse::<f64>().is_err() {
            println!("INCORRECT INPUT TRY AGAIN");
            input_string.clear();
            io::stdin().read_line(&mut input_string).unwrap();
        }
        lose_mult = input_string.trim().parse::<f64>().unwrap();

        betting_strategy = mylib::BetStrategy::new(starting_bet, max_bet, lose_mult);
        println!(
            "BETTING: STARTING BET: {}, MAX_BET: {}, LOSE_MULT: {}",
            starting_bet, max_bet, lose_mult
        );
    }

    // run sim with chosen settings
    result_vec = my_deck.sim(betting_strategy, move_rules, n_decks, n_hands, start_chips);
    // display sim results
    println!("STARTING CHIPS: {}", start_chips);
    for i in result_vec.clone() {
        println!("Hand Result: {:?}, Chips: {}", i.1, i.0);
    }
}

//! RustJack Blackjack sim
//! Automate hands of blackjack with
//!     customizable move rules and
//!     betting strategy
//! Author: Drew McKinney
//! Date: 2/10/22

extern crate ndarray;
use rand::seq::SliceRandom;

/// CardType type for storing different cards in deck
#[derive(Debug, Clone, PartialEq)]
pub enum CardType {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

/// Card
/// Cards have a CardType, a value,
///     and a bool for whether they are soft
///     values or not
#[derive(Debug, Clone, PartialEq)]
pub struct Card {
    card_name: CardType,
    card_value: u64,
    is_soft: bool,
}

impl Card {
    // create new card with name and value
    pub fn new(name: CardType, value: u64) -> Self {
        let c_name = name;
        let c_value = value;
        let mut c_soft = false;
        if value == 11 {
            c_soft = true;
        }

        Card {
            card_name: c_name,
            card_value: c_value,
            is_soft: c_soft,
        }
    }

    // get hard value for when soft hand goes bust
    pub fn to_hard(&mut self) {
        if self.is_soft {
            self.card_value -= 10;
            self.is_soft = false;
        }
    }
}

/// Hands have a list of cards, a total value
///     a bool for if they have gone bust or not
///     and a bool for if they are blackjack or not
pub struct Hand {
    card_list: Vec<Card>,
    hand_total: u64,
    //soft_total: u64,
    //is_soft: bool,
    is_busted: bool,
    //is_blackjack: bool,
}

impl Hand {
    //create empty hand
    pub fn new() -> Self {
        let card_list = Vec::new();
        let hand_total = 0;
        //let soft_total = 0;
        //let is_soft = false;
        let is_busted = false;
        //let is_blackjack = false;
        Hand {
            card_list,
            hand_total,
            //soft_total,
            //is_soft,
            is_busted,
            //is_blackjack,
        }
    }

    /// deal card to hand
    /// returns true if busted, false if not
    pub fn deal(&mut self, new_card: Card) -> bool {
        let dealt = new_card.clone();
        let new_card_value = new_card.card_value;
        //let new_card_name = new_card.card_name.clone();

        // add card to card list
        self.card_list.push(dealt);

        // increase hand value
        self.hand_total += new_card_value;

        // check if busted
        if self.hand_total > 21 {
            // check if soft and adjust total accordingly
            let new_total = self.check_for_softs();
            if new_total > 21 {
                // bust
                self.is_busted = true;
            }
            /*
            if self.is_soft {
                self.hand_total -= 10;
                self.is_soft = false;
                if self.hand_total > 21 {
                    self.is_busted = true;
                }
            } else {
                // bust
                self.is_busted = true;
            }
            */
        }
        self.is_busted
    }

    /// check each hand in card until soft ace is found
    /// change ace to hard value and correct hand total
    /// return updated hand_total
    pub fn check_for_softs(&mut self) -> u64 {
        for c in &mut self.card_list {
            if c.is_soft {
                c.to_hard();
                self.hand_total -= 10;
                return self.hand_total;
            }
        }
        self.hand_total
    }

    pub fn has_blackjack(&mut self) -> bool {
        if self.hand_total == 21 {
            for c in &mut self.card_list {
                if c.card_name == CardType::Ace {
                    return true;
                }
            }
        }
        false
    }
}
impl Default for Hand {
    fn default() -> Self {
        Self::new()
    }
}

/// types of moves,
/// includes Hit, Stand, Double, Split
#[derive(Debug, Clone, PartialEq)]
pub enum Move {
    Hit,
    Stand,
    Double,
    Split,
}

/// Rules
/// Contains a matrix for each possible
/// combination of player/dealer values
/// that contains Move types for automation
#[derive(Debug, Clone, PartialEq)]
pub struct Rules {
    totals: [[Move; 10]; 10],
    //pair_splitting: [[Move; 10]; 10],
}

impl Rules {
    // default rules
    pub fn new() -> Self {
        let h = Move::Hit;
        let s = Move::Stand;
        let d = Move::Double;

        let totals_rules = [
            // 4-8 in hand
            [
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
            ],
            // 9
            [
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
            ],
            // 10
            [
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                h.clone(),
                h.clone(),
            ],
            // 11
            [
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d.clone(),
                d,
            ],
            // 12
            [
                h.clone(),
                h.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
            ],
            // 13
            [
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
            ],
            // 14
            [
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
            ],
            // 15
            [
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
            ],
            // 16
            [
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h.clone(),
                h,
            ],
            // 17-21
            [
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s.clone(),
                s,
            ],
        ];

        /*
        //[[def_move; 10]; 10];
        let pair_split_rules = [
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
            [
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
                m.clone(),
            ],
        ];
        */
        Rules {
            totals: totals_rules,
            //pair_splitting: pair_split_rules,
        }
    }

    /// change_rule
    /// changes automated rule setting
    /// based on user input
    pub fn change_rule(&mut self, p_total: u64, d_total: u64, move_type: Move) {
        // position in rule matrix to change
        let pos = self.get_pos(p_total, d_total);
        let x = pos.0 as usize;
        let y = pos.1 as usize;

        self.totals[x][y] = move_type;
    }

    /// get_pos
    /// gets actual matrix index for given
    /// player/dealer hand value combo
    /// returns tuple of indexing values
    pub fn get_pos(&mut self, p_total: u64, d_total: u64) -> (usize, usize) {
        let x;
        let y;
        // determine x and y values of move matrix
        match p_total {
            4..=8 => x = 0,
            9 => x = 1,
            10 => x = 2,
            11 => x = 3,
            12 => x = 4,
            13 => x = 5,
            14 => x = 6,
            15 => x = 7,
            16 => x = 8,
            17..=21 => x = 9,
            _ => x = 10,
        }
        match d_total {
            2 => y = 0,
            3 => y = 1,
            4 => y = 2,
            5 => y = 3,
            6 => y = 4,
            7 => y = 5,
            8 => y = 6,
            9 => y = 7,
            10 => y = 8,
            11 => y = 9,
            _ => y = 10,
        }
        (x, y)
    }

    /// det_move
    /// determines which move to play
    /// based on move rule matrix and given index
    /// value pair
    /// returns move found at index
    pub fn det_move(&mut self, p_total: u64, d_total: u64) -> Move {
        let pos = self.get_pos(p_total, d_total);
        let x = pos.0 as usize;
        let y = pos.1 as usize;

        self.totals[x][y].clone()
    }
}
impl Default for Rules {
    fn default() -> Self {
        Self::new()
    }
}

/// Result type
/// Hand can be either a win, lose, push
/// or blackjack
#[derive(Debug, Clone, PartialEq)]
pub enum Result {
    Win,
    Lose,
    Push,
    Blackjack,
}

/// BetStrategy
/// contains info for automated betting
/// starting bet, max, bet, lost hand multiplier
#[derive(Debug, Clone)]
pub struct BetStrategy {
    start_bet: u64,
    max_bet: u64,
    lose_mult: f64,
    //push_mult: u64,
}

impl BetStrategy {
    // default betting strat
    pub fn new(starting_bet: u64, max_bet: u64, lose_mult: f64) -> Self {
        let start = starting_bet;
        let max = max_bet;
        let l_mult = lose_mult;
        //let p_mult = 1;

        BetStrategy {
            start_bet: start,
            max_bet: max,
            lose_mult: l_mult,
            //push_mult: p_mult,
        }
    }

    /// new bet
    /// determines new bet value based
    /// on previous bet and outcome of
    /// previous hand
    pub fn new_bet(&mut self, last_bet: u64, last_res: Result) -> u64 {
        let mut new_bet = last_bet;
        if last_res == Result::Win || last_res == Result::Blackjack {
            self.start_bet
        } else if last_res == Result::Lose {
            if ((last_bet as f64 * self.lose_mult) as u64) < self.max_bet {
                new_bet = (last_bet as f64 * self.lose_mult) as u64;
            }
            new_bet
        } else {
            new_bet
        }
    }
}

/// Deck
/// Contains vector of cards, deck length,
/// and number of decks used
#[derive(Debug, Clone, PartialEq)]
pub struct Deck {
    deck_cards: Vec<Card>,
    deck_length: u64,
    deck_mult: u64,
}

impl Deck {
    // default deck
    pub fn new(deck_count: u64) -> Self {
        let mult = deck_count * 4;
        let mut new_deck = Vec::new();
        for _i in 0..mult {
            let mut new_card = Card::new(CardType::Two, 2);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Three, 3);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Four, 4);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Five, 5);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Six, 6);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Seven, 7);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Eight, 8);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Nine, 9);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Ten, 10);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Jack, 10);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Queen, 10);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::King, 10);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Ace, 11);
            new_deck.push(new_card.clone());
        }

        let mut rng = rand::thread_rng();
        new_deck.shuffle(&mut rng);

        Deck {
            deck_cards: new_deck.clone(),
            deck_length: new_deck.clone().len() as u64,
            deck_mult: deck_count,
        }
    }

    /// deal_from_deck
    /// deals a card from the top of deck
    /// returns Card from deck
    pub fn deal_from_deck(&mut self) -> Card {
        if self.deck_length == 0 {
            // re-shuffle deck
            // shuffle
            self.reshuffle();
        }
        // pop card off deck
        let new_card = self.deck_cards.pop().unwrap();
        self.deck_length -= 1;
        new_card
    }

    /// reshuffle
    /// re shuffle random deck with given deck parameters
    pub fn reshuffle(&mut self) {
        let mult = self.deck_mult * 4;
        let mut new_deck = Vec::new();
        for _i in 0..mult {
            let mut new_card = Card::new(CardType::Two, 2);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Three, 3);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Four, 4);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Five, 5);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Six, 6);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Seven, 7);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Eight, 8);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Nine, 9);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Ten, 10);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Jack, 10);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Queen, 10);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::King, 10);
            new_deck.push(new_card.clone());
            new_card = Card::new(CardType::Ace, 11);
            new_deck.push(new_card.clone());
        }

        let mut rng = rand::thread_rng();
        new_deck.shuffle(&mut rng);

        self.deck_cards = new_deck.clone();
        self.deck_length = new_deck.len() as u64;
    }
    /// sim
    /// runs automated Rustjack sim
    /// takes BetStrategy, Rules,
    ///     n_decks, n_runs, and start_chips
    /// returns Vector of tuples
    ///     containing result of hand and new chip count
    pub fn sim(
        &mut self,
        betting: BetStrategy,
        move_rules: Rules,
        n_decks: u64,
        n_runs: u64,
        start_chips: u64,
    ) -> Vec<(u64, Result)> {
        let mut res = Vec::new();
        // res.push((1, Result::Win));

        // use rules and betting strategy
        // automate betting and moves
        // simulate number of hands or until
        //      out of money

        // repeat simulation for given
        //      number of hands
        let mut sim_deck = Deck::new(n_decks);
        let mut curr_chips = start_chips;
        let mut curr_bet = betting.start_bet;
        let mut res_last_hand = Result::Win;

        for _run in 0..n_runs {
            let mut player_blackjack = false;
            // calculate bet for current hand
            curr_bet = betting.clone().new_bet(curr_bet, res_last_hand.clone());
            //println!("{}", curr_bet);

            if curr_chips < curr_bet {
                return res;
            }
            curr_chips -= curr_bet;

            // declare player and dealer hands
            // dealer hand
            let mut dealer = Hand::new();
            // player hand
            let mut player = Hand::new();

            // deal card to player
            let mut c = sim_deck.deal_from_deck().clone();
            player.deal(c.clone());
            //println!("Player first dealt: {:?}", c.card_name);

            // deal card to dealer
            c = sim_deck.deal_from_deck().clone();
            dealer.deal(c.clone());
            //println!("Dealer first dealt: {:?}", c.card_name);

            // deal second card to player
            c = sim_deck.deal_from_deck().clone();
            player.deal(c.clone());
            if player.has_blackjack() {
                player_blackjack = true;
            }

            // get secret dealer second card
            let hidden_card = sim_deck.deal_from_deck().clone();

            // determine player move
            // repeat until busted or move is stand or double
            let mut last_move = Move::Hit;
            let mut curr_move = Move::Hit;
            while !player.is_busted
                && curr_move != Move::Stand
                && last_move != Move::Double
                && !player_blackjack
            {
                curr_move = move_rules
                    .clone()
                    .det_move(player.hand_total, dealer.hand_total);
                // move is hit
                if curr_move == Move::Hit {
                    let new_card = sim_deck.deal_from_deck().clone();
                    player.deal(new_card);
                    last_move = curr_move.clone();
                }
                // move is double
                else if curr_move == Move::Double {
                    // can double
                    if curr_bet * 2 < curr_chips {
                        curr_move = Move::Double;
                        curr_chips -= curr_bet;
                        curr_bet *= 2;
                        //println!("DOUBLED DOWN! NEW BET IS: {}", curr_bet);
                    }
                    // can't double
                    else {
                        curr_move = Move::Hit;
                    }
                    let new_card = sim_deck.deal_from_deck().clone();
                    player.deal(new_card);
                    last_move = curr_move.clone();
                }
                // move is stand
                else if curr_move == Move::Stand {
                    // do nothing else
                }
            }
            let player_total = player.hand_total;

            // determine dealer move
            // deal hidden dealer card
            dealer.deal(hidden_card.clone());
            let mut dealer_total = dealer.hand_total;
            while !dealer.is_busted && dealer_total < 17 {
                // deal new card to dealer
                let new_card = sim_deck.deal_from_deck().clone();
                dealer.deal(new_card);
                dealer_total = dealer.hand_total;
            }

            // determine result of hand
            // player has blackjack
            if player_blackjack {
                if dealer_total == 21 {
                    curr_chips += curr_bet;
                    res.push((curr_chips, Result::Push));
                    res_last_hand = Result::Push;
                } else {
                    let winnings = curr_bet as f64 * 2.5;
                    curr_chips += winnings as u64;
                    res.push((curr_chips, Result::Blackjack));
                    res_last_hand = Result::Blackjack;
                }
            }
            // player busts, lose
            if player.is_busted {
                res.push((curr_chips, Result::Lose));
                res_last_hand = Result::Lose;
            }
            // dealer busts and not player, win
            else if dealer.is_busted {
                curr_chips += curr_bet * 2;
                res.push((curr_chips, Result::Win));
                res_last_hand = Result::Win;
            }
            // dealer and player push
            else if player_total == dealer_total {
                curr_chips += curr_bet;
                res.push((curr_chips, Result::Push));
                res_last_hand = Result::Push;
            }
            // dealer has higher total, lose
            else if player_total < dealer_total {
                res.push((curr_chips, Result::Lose));
                res_last_hand = Result::Lose;
            }
            // player has higher total, win
            else if player_total > dealer_total {
                curr_chips += curr_bet * 2;
                res.push((curr_chips, Result::Win));
                res_last_hand = Result::Win;
            }
        }
        res
    }
}

#[cfg(test)]
mod my_tests;

//! Tests for rustjack!
//! Author: Drew McKinney
//!

use super::BetStrategy;
use super::Card;
use super::CardType;
use super::Deck;
use super::Hand;
use super::Move;
use super::Result;
use super::Rules;

#[test]
fn card_test() {
    let name = CardType::Ace;
    let new_card = Card::new(name.clone(), 1);
    assert_eq!(new_card.card_name, name);
    assert_eq!(new_card.card_value, 1);
}

#[test]
fn hand_test() {
    let mut new_hand = Hand::new();
    //let new_hand = &mut _hand;

    assert_eq!(new_hand.card_list.clone().len(), 0);
    assert_eq!(new_hand.hand_total.clone(), 0);
    let _busted = new_hand.is_busted.clone();
    assert_eq!(_busted, false);
    //assert_eq!(new_hand.is_soft.clone(), false);

    let name = CardType::Ace;
    let ace_value = 11;
    let new_card = Card::new(name.clone(), ace_value);
    let mut hand_busted = new_hand.deal(new_card.clone());
    assert_eq!(new_hand.hand_total.clone(), ace_value);
    assert_eq!(hand_busted, false);
    //assert_eq!(new_hand.is_soft.clone(), true);
    let _busted = new_hand.is_busted.clone();
    assert_eq!(new_hand.is_busted.clone(), false);

    let name = CardType::King;
    let face_value = 10;
    let new_card = Card::new(name.clone(), face_value);
    hand_busted = new_hand.deal(new_card.clone());
    assert_eq!(new_hand.hand_total, face_value + ace_value);
    assert_eq!(new_hand.is_busted, false);
    assert_eq!(new_hand.is_busted, hand_busted);

    hand_busted = new_hand.deal(new_card.clone());
    assert_eq!(
        new_hand.hand_total,
        (face_value + face_value + ace_value - 10)
    );
    assert_eq!(hand_busted, false);
    assert_eq!(new_hand.is_busted, hand_busted);

    hand_busted = new_hand.deal(new_card.clone());
    assert_eq!(
        new_hand.hand_total,
        (face_value + face_value + face_value + ace_value - 10)
    );
    assert_eq!(new_hand.is_busted, true);
    assert_eq!(new_hand.is_busted, hand_busted);
}

#[test]
fn rules_test() {
    let mut my_rules = Rules::new();
    assert_eq!(my_rules.totals[0][2].clone(), Move::Hit);
    assert_eq!(my_rules.totals[2][7].clone(), Move::Double);
    assert_eq!(my_rules.totals[2][8].clone(), Move::Hit);
    assert_eq!(my_rules.totals[4][2].clone(), Move::Stand);

    let (x, y) = my_rules.get_pos(4, 2);
    assert_eq!(my_rules.totals[x][y], Move::Hit);
    my_rules.change_rule(4, 2, Move::Stand);
    assert_eq!(my_rules.totals[x][y], Move::Stand);
}

#[test]
fn bet_test() {
    let my_bet_strat = BetStrategy::new(10, 300, 2.0);
    assert_eq!(my_bet_strat.start_bet.clone(), 10);
    assert_eq!(my_bet_strat.lose_mult.clone(), 2.0);
    let my_bet = 25;
    let my_result = Result::Lose;
    assert_eq!(my_bet_strat.clone().new_bet(my_bet, my_result), 50);
}

#[test]
fn deck_test() {
    let deck_mult = 2;
    let my_deck = Deck::new(deck_mult);
    for _i in my_deck.deck_cards.clone() {
        //println!("{}", i.card_value.clone());
    }
    assert_eq!(my_deck.deck_length.clone(), 52 * deck_mult);
}

#[test]
#[warn(dead_code)]
fn sim_test() {
    let mut res_v: Vec<(u64, Result)> = Vec::new();

    let starting_bet = 5;
    let bet_strat = BetStrategy::new(starting_bet, 300, 2.0);
    let move_rules = Rules::new();
    let n_decks = 2;
    let n_hands = 20;
    let chips = 1000;
    let my_deck = Deck::new(n_decks);
    res_v = my_deck
        .clone()
        .sim(bet_strat.clone(), move_rules, n_decks, n_hands, chips);
    // display sim results
    println!("STARTING CHIPS: {}", chips);
    for i in res_v.clone() {
        println!("Chips: {}, Hand Result: {:?}", i.0, i.1);
    }

    println!("{}", res_v.len());
    //assert_eq!(res_v.clone().len() as u64, n_hands);
}
